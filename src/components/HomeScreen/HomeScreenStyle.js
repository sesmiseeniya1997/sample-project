import {
    PixelRatio,
    StyleSheet,
} from 'react-native';
import Colors from '../../resources/Colors';

const styles = StyleSheet.create({
    menuViewStyles: {
        width: 40,
        height: 40,
        marginLeft: 20,
        marginTop: 10,
        backgroundColor: Colors.secondary_color,
        borderTopRightRadius: 10,
        borderBottomRightRadius: 10,
        borderBottomLeftRadius: 10,
    },

    menuViewImageStyles: {
        width: 27,
        height: 27,
        marginTop: 5,
        marginLeft: 5,
    },

    mainTextStyles: {
        fontWeight: 'bold',
        fontSize: 30,
        left: 20,
        top: 30,
    },

    subText1Styles: {
        color: Colors.primaryColor,
        fontSize: 20,
        left: 20,
        top: 25,
    },

    chair2ImageStyles: {
        width: 150,
        height: 240,
        marginTop: 50,
        marginLeft: 125,
        borderTopRightRadius: 25,
        borderBottomLeftRadius: 25,
    },

    chair3ImageStyles: {
        width: 90,
        height: 200,
        marginTop: -220,
        marginLeft: -10,
        borderTopRightRadius: 25,
        borderBottomLeftRadius: 25,
    },

    chair1ImageStyles: {
        width: 90,
        height: 200,
        marginTop: -200,
        marginLeft: 310,
        borderTopRightRadius: 25,
        borderBottomLeftRadius: 25,
    },

    subText2Styles: {
        fontWeight: 'bold',
        fontSize: 20,
        left: 20,
        top: 50,
    },

    subText3Styles: {
        color: Colors.primaryColor,
        fontSize: 16,
        marginLeft: 20,
        marginTop: 55,
    },

    button1Styles: {
        width: 180,
        height: 55,
        backgroundColor: Colors.secondary_color,
        borderTopRightRadius: 0,
        borderBottomLeftRadius: 15,
        borderTopLeftRadius: 15,
        borderBottomRightRadius: 30
    },

    button1TextStyle: {
        color: Colors.textColor,
        fontSize: 20,
        fontWeight: 'bold',
        marginLeft: 70,
        marginTop: 10,
    },

    button2Styles: {
        marginLeft: 190,
        marginTop: -55,
        width: 180,
        height: 55,
        backgroundColor: Colors.secondary_color,
        borderTopRightRadius: 15,
        borderBottomLeftRadius: 30,
        borderTopLeftRadius: 0,
        borderBottomRightRadius: 25
    },

    button2TextStyles: {
        color: Colors.textColor,
        fontSize: 20,
        fontWeight: 'bold',
        marginLeft: 70,
        marginTop: 10,
    },
})

export default styles;